﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Globalization;

namespace WindowLogger
{
    public partial class Form1 : Form
    {
        [DllImport("user32.dll")]
        static extern IntPtr GetForegroundWindow();

        App lastApplication;
        DateTime lastTime;

        [DllImport("user32.dll")]
        static extern int GetWindowText(IntPtr hWnd, StringBuilder text, int count);

        String quitstring = "";
        Boolean formCanBeClosed = false;
        
        List<App> apps = new List<App>();
        DateTime currentTime;




        public Form1()
        {            
            InitializeComponent();
            
            lastTime = DateTime.Now;
            currentTime = DateTime.Now;
            treeView1.ExpandAll();
            lastApplication = new App(GetForegroundWindow());
            Timer interval = new Timer();
            interval.Interval = 1000;
            interval.Tick += new EventHandler(t_Tick);
            interval.Start();

        }


        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (formCanBeClosed)
            {
                var closeMsg = MessageBox.Show("Do you really want to close?", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (closeMsg == DialogResult.Yes)
                {
                    // do nothing

                }
                else
                {
                    e.Cancel = true;
                    formCanBeClosed = false;
                }
            }
            else
            {
                e.Cancel = true;
                Visible = false; // Hide form window.
                ShowInTaskbar = false; // Remove from taskbar.
            }
            
        }



        void t_Tick(object sender, EventArgs e)
        {
            currentTime = DateTime.Now;
            App currentActiveApp = new App(GetForegroundWindow());
            App applicationIsKnown = apps.Find(x => x.Name == currentActiveApp.Name);

            if (applicationIsKnown == null)
            {
                if (currentActiveApp.Name.Length > 0)
                {
                    apps.Add(currentActiveApp);
                    appendApplicationToLog(currentActiveApp);
                    applicationIsKnown = apps.Find(x => x.Name == currentActiveApp.Name);
                    TreeNode newNode = new TreeNode(applicationIsKnown.Name);
                    treeView1.Nodes.Add(newNode);
                }
            }
            if (applicationIsKnown != null)
            {
                TimeSpan span = currentTime.Subtract(lastTime);
                applicationIsKnown.increaseActiveTime(span.TotalSeconds);
                lastTime = currentTime;
                appendApplicationToLog(applicationIsKnown);
                refreshTimeInTree(applicationIsKnown);
            }
            else
            {
                appendApplicationToLog(currentActiveApp);
            }
            
                lastApplication = applicationIsKnown;
        }

        private void refreshTreeComplete()
        {
            // Suppress repainting the TreeView until all the objects have been created.
            treeView1.BeginUpdate();

            // Clear the TreeView each time the method is called.
            treeView1.Nodes.Clear();

            // Add a root TreeNode for each Customer object in the ArrayList.
            foreach (App app in apps)
            {
                treeView1.Nodes.Add(new TreeNode(app.Name));

                treeView1.Nodes[apps.IndexOf(app)].Nodes.Add(
                  new TreeNode("Active: " + app.getActiveTimeAsString()));
                treeView1.Nodes[apps.IndexOf(app)].Nodes.Add(
                       new TreeNode("Inactive: " + app.getInactiveTimeAsString()));

            }
            // Begin repainting the TreeView.
            treeView1.EndUpdate();

        }

        private void refreshTimeInTree(App app)
        {
                treeView1.Nodes[apps.IndexOf(app)].Nodes.Clear();
                treeView1.Nodes[apps.IndexOf(app)].Nodes.Add(
                  new TreeNode("Active: " + app.getActiveTimeAsString()));
                treeView1.Nodes[apps.IndexOf(app)].Nodes.Add(
                       new TreeNode("Inactive: " + app.getInactiveTimeAsString()));
        }

        private void appendApplicationToLog(App currentActiveApp)
        {
            if (lastApplication != null && lastApplication.Name != currentActiveApp.Name)
            {
                richTextBox1.Text += currentTime.ToString("dd.MM.yyyy HH:mm", CultureInfo.CreateSpecificCulture("de-DE")) + "\t" + currentActiveApp.Name + "\n";
            }

        }
        

        private void button1_Click(object sender, EventArgs e)
        {
            richTextBox1.Text = "";
                
        }

        private void notifyIcon1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            Visible = true; // Hide form window.
            ShowInTaskbar = true; // Remove from taskbar.
        }

        private void keypress(object sender, KeyPressEventArgs e)
        {
            string codeword = "quit";
            if (e.KeyChar.ToString() == codeword.Substring(quitstring.Length,1))
            {
                quitstring += e.KeyChar.ToString();
                if (codeword == quitstring)
                {
                    quitstring = "";
                    formCanBeClosed =true;
                    this.Close();
                }
            } else
            {
                quitstring = "";
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Visible = false; // Hide form window.
            ShowInTaskbar = false; // Remove from taskbar.
        }
    }

    public class App
    {
        [DllImport("user32.dll")]
        static extern int GetWindowText(IntPtr hWnd, StringBuilder text, int count);

        public String Name { get; set; }
        public Double ActiveSeconds { get; set; }
        public Double InactiveSeconds { get; set; }
        public String Pid { get; set; }
        public DateTime lastSeen { get; set; }

        const int nChars = 256;
        StringBuilder Buff = new StringBuilder(nChars);
        public App(IntPtr currentWindow)
        {
            GetWindowText(currentWindow, Buff, nChars);
            this.Name = Buff.ToString();
            this.Pid = currentWindow.ToString();
        }
        public void increaseActiveTime(Double additionalTime)
        {
            if (IdleTimeDetector.IdleSince.TotalSeconds >= 180)
            {
                this.InactiveSeconds += additionalTime;
            }
            else
            {
                this.ActiveSeconds += additionalTime;
            }
        }
        public String getActiveTimeAsString()
        {
            int stunden = 0, minuten = 0, sekunden = 0;
            stunden = (int)((int)this.ActiveSeconds / (3600));
            minuten = (int)(((int)this.ActiveSeconds / (60)) - (stunden * 60));
            sekunden = (int)((this.ActiveSeconds - (stunden * 3600)) - (minuten * 60));
            TimeSpan time = new TimeSpan(stunden, minuten, sekunden);
            return time.ToString(@"hh\:mm\:ss");
        }
        public String getInactiveTimeAsString()
        {
            int stunden = 0, minuten = 0, sekunden = 0;
            stunden = (int)((int)this.InactiveSeconds / (3600));
            minuten = (int)(((int)this.InactiveSeconds / (60)) - (stunden * 60));
            sekunden = (int)((this.InactiveSeconds - (stunden * 3600)) - (minuten * 60));
            TimeSpan time = new TimeSpan(stunden, minuten, sekunden);
            return time.ToString(@"hh\:mm\:ss");
        }

    }
    public static class IdleTimeDetector
    {
        [DllImport("user32.dll")]
        static extern bool GetLastInputInfo(out LastInputInfo plii);

        [StructLayout(LayoutKind.Sequential)]
        struct LastInputInfo
        {
            public static readonly int SizeOf = Marshal.SizeOf(typeof(LastInputInfo));
            [MarshalAs(UnmanagedType.U4)]
            public int cbSize;
            [MarshalAs(UnmanagedType.U4)]
            public int dwTime;
        }

        public static TimeSpan IdleSince
        {
            get
            {
                int idleTime = 0;
                LastInputInfo lastInputInfo = new LastInputInfo();
                lastInputInfo.cbSize = Marshal.SizeOf(lastInputInfo);
                lastInputInfo.dwTime = 0;

                if (GetLastInputInfo(out lastInputInfo))
                {
                    idleTime = Environment.TickCount - lastInputInfo.dwTime;
                }

                return TimeSpan.FromMilliseconds(idleTime);
            }
        }
    }
}
